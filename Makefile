MODULE=driver_DHT11
 
KERNEL_SRC=/lib/modules/`uname -r`/build
 
obj-m += ${MODULE}.o
 
compile:
	make -C ${KERNEL_SRC} M=${CURDIR} modules

install: 
	sudo insmod ${MODULE}.ko 
	dmesg | tail 
	sudo chmod go+r /dev/DHT11
	
uninstall:
	sudo rmmod ${MODULE} 
	dmesg | tail
 
 
 
