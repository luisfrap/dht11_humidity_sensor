#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/timer.h>
#include <linux/init.h>
#include <linux/miscdevice.h> // misc dev
#include <linux/fs.h>         // file operations
#include <asm/uaccess.h>      // copy to/from user space
#include <linux/wait.h>       // waiting queue
#include <linux/sched.h>      // TASK_INTERRUMPIBLE
#include <linux/delay.h>      // udelay
#include <linux/io.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <linux/slab.h>

#define DRIVER_AUTHOR "AZUL"
#define DRIVER_DESC   "Driver para DHT11"
#define DHT_GPIO 18		//Usaremos el pin 18 como el bus serie de datos
#define DHT_GPIO_DESC 		 "Pin de la ES DHT11 en la plaquita"
#define DHT_GPIO_DEVICE_DESC "Sensor de temperatura-humedad DHT11"
/* El tamaño del biffer. Actucúa como un múltiplo para el tamaño de cada buffer. */
#define BUFFER_SIZE 64
/* Definiciones para medidas del tiempo. */
#define ERROR 10  //ticks de reloj a 16 MHz
#define TIEMPOCERO 77 //ticks de reloj a 16 MHz
#define TIEMPOUNO 123 //ticks de reloj a 16 MHz
/* Direccion del timer de 16MHz = PI2_TIMER_PAGE_BASE+TIMER_OFFSET */
#define PI2_TIMER_PAGE_BASE 0x3f003000  // para modelos RPI2 y RPI3 con 1024MB de RAM
#define TIMER_OFFSET 4  // común

static unsigned int irq_DHT = 0;			//Identificador de interrupción

static irqreturn_t irq_handler_DHT(void);		//Manejador de la interrupción 

/* Declaración de buffers. */
/* En este buffer guardamos los tiempos que corresponden a 1s o a 0s */
static u32 *dht_buffer = NULL;	
/* En este buffer guardamos los valores transformados que vamos a pasar al buffer del usuario. */
static char *str_buffer = NULL;
/*Este buffer guarda el valor en binario de los ticks recibidos por el sensor*/	
static char *bits = NULL;
/* El puntero para el buffer. */	
static unsigned p_buffer;


/* El puntero para mapear el timer de 16 MHz */
static void *timer;
/* Variable auxiliar para guardar el último valor del timer. */
static u32 cur_timer;

/*Este semáforo mantiene en espera al proceso mientras se recogen los datos	*/
DEFINE_SEMAPHORE(sem_wait_processing); 
/*El semáforo para controlar la zona crítica (variable working) */
DEFINE_SEMAPHORE(critical_section); 

/*Inicialización de variables */
static unsigned humedad_e;
static unsigned temperatura_e;
static unsigned humedad_d;
static unsigned temperatura_d;
static unsigned checksum;
static char *status[2] = {"WRONG", "OK"};
/*La variable working la usaremos como control para devolver los datos de una
 * lectura anterior cuando el proceso aún está recibiendo los datos	*/
static char working;

/****************************************************************************/
/* TRANSLATE                                     							*/
/****************************************************************************/

/*
Funcion para crear la potencia de un numero, así conseguimos convertir de binario a decimal
*/
static int powk(int num, int pow){
	int res=1;
	for(; pow > 0; pow--)
		res *= num;
		
	return res;
}


/*
Clasificamos el dato en alto o bajo, para ello comprobamos si el valor obtenido en ticks[pos] es menor que el tiempo para un '0'
si es así ponemos un 0  en la posición pos de bits[], en caso contrario ponemos un 1
Nota: Al realizar la lectura, los dos primeros valores son "incorrectos" por tanto empezamos la lectura en el tercer dato que leemos
a causa de eso 'pos' se inicializa en 2. El indice de bits[pos-2] se resta para que se almacene en la primera (y sucesivas) posiciones
del array en binario
*/

static void buff2bin(void){ 
	int pos = 2;
	while(pos < 42){
		if(dht_buffer[pos] < (TIEMPOCERO+ERROR)){
			bits[pos-2] = 0;
		}else{
			bits[pos-2] = 1;
		}
		pos++;
	}	
}

/*
Separamos en 5 variables el array obtenido en el paso anterior y pasamos de binario a decimal.
Para ello multiplicamos el bit de la posicion 'i' por su potencia base 2 correspondiente a su posicion.
Por ejemplo, el primer bit actuaría como sigue:
	bits[0] = 1; humedad_e=1*powk(2^(7-1)) == 64
	A partir de la segunda variable en powk le restamos la menor posicion del indice en ese punto para que se mantenga la potencia entre 0 y 7
*/


static void bin2dec(void){
	int i;
	
	for(i=0;i<40;i++){		
		if(i<8){
			humedad_e += bits[i]*powk(2,7-i);
	    } else if(i < 16){
			humedad_d += bits[i]*powk(2,7-(i-8));
		} else if((i<24)){
			temperatura_e += bits[i]*powk(2,7-(i-16));
		} else if(i<32){
			temperatura_d += bits[i]*powk(2,7-(i-24));
		} else {
			checksum += bits[i]*powk(2, 7-(i-32));
		}
	}
}

/*
Comprobamos si la suma de los 4 segmentos es igual a la del checksum, si es así ponemos un 1
*/

static void sumaCheck(void){
	checksum = ((temperatura_d+temperatura_e+humedad_d+humedad_e) == checksum);
}

/****************************************************************************/
/*Libera el proceso cuando se han recibido los datos para empezar las conversiones	*/
static void f_timerHandler(unsigned long data){
	gpio_set_value(DHT_GPIO, 1);
	udelay(40);
	gpio_direction_input(DHT_GPIO);
	enable_irq(irq_DHT);
}

/* Definimos el temporizador */
DEFINE_TIMER(timer_handler, f_timerHandler, 0, 0);

/*Esta es la función principal de lectura*/
static ssize_t dht_read (struct file *filp, char __user *buf, size_t count, loff_t *f_pos){
	/*La variable cnt almacena el número de bytes que pasamos al buffer de usuario*/
	int cnt = 0;
	/*	Esta sentencia controla que la lectura de valores no sea infinita */
	if(*f_pos > 0) return 0;
	
	/*Si se piden los datos mientras aún se están procesando se devolverán
	 * los datos de la lectura anterior. 
	 * No olvidamos de controlar la sección crítica, la variable working para
	 * dos procesos independientes no entren por casualidad a la vez. */
	down(&critical_section);
	if(working){
		cnt = sprintf(str_buffer, "%u.%u ºC, %u.%u %%, checksum %s\n", temperatura_e, temperatura_d, humedad_e, humedad_d, status[checksum]);
		if(copy_to_user(buf, str_buffer, cnt))
			return -EFAULT;
		return cnt;
	}
	/*El proceso empieza y cambiamos la variable a 1*/
	working = 1;
	/*Soltamos el semáforo después de haber usado el recurso compartido. */
	up(&critical_section);
	
	/* Inicio del protocolo con el DHT , bajamos el bus a 0 
	 * y hacemos una espera de 25 ms como indica el datasheet */
	gpio_set_value(DHT_GPIO, 0);
	mod_timer(&timer_handler, jiffies + HZ*25/1000); 
	
	/* Y nos quedamos esperando hasta que los datos estén listos para procesarlos */
	if (down_interruptible(&sem_wait_processing)){
		return -ERESTARTSYS;
	}
	
	/*Llamamos a las fuciones de procesamiento de los datos */
	buff2bin();
	bin2dec();
	sumaCheck();
	
	/*Una vez que tenemos los datos procesados los pasamos al buffer intermedio del driver */
	cnt = sprintf(str_buffer, "%u.%u ºC, %u.%u %%, checksum %s\n", temperatura_e, temperatura_d, humedad_e, humedad_d, status[checksum]);
	/*Copiamos los datos del buffer anterior al buffer de usuario. En este punto
	 * se realiza la intersección entre las capas de KERNEL Lynux y el usuario. */
	if(copy_to_user(buf, str_buffer, cnt)){
		return -EFAULT;
	}
	
	/* Finalizamos la comunicación con el sensor poniendo el bus a 1 */
    gpio_direction_output(DHT_GPIO, 1);
    
    /*Desplazamos el puntero de lectura del fichero del sensor*/
    *f_pos += cnt;
	
	/*Reseteamos las variables para la próxima lectura	*/
	p_buffer = 0;	
	humedad_e = 0;
	temperatura_e = 0;
	humedad_d = 0;
	temperatura_d = 0;
	checksum = 0;
	
	/*Como hemos finalizado la comunicación indicamos que el driver ha acabado el procesamiento */
	working = 0;
    
    /*Devolvemos el número de bytes escritos en el buffer de usuario */
    return cnt;
}

static const struct file_operations DHT_fops = {
    .owner	= THIS_MODULE,
    .read	= dht_read
};

/****************************************************************************/
/* DHT11's device struct                                                       */

static struct miscdevice DHT_miscdev = {
    .minor	= MISC_DYNAMIC_MINOR,
    .name	= "DHT11",
    .fops	= &DHT_fops,
};

/*****************************************************************************/
/* This functions registers devices, requests GPIOs and configures interrupts */
/*****************************************************************************/

/*******************************
 *  register device for DHT11
 *******************************/

static int device_config(void) {
    int ret=0;
    
    if ((ret = misc_register(&DHT_miscdev))) {
        printk(KERN_ERR "DHT_misc_register failed\n");
    }
	else
		printk(KERN_NOTICE "DHT_misc_register OK... leds_miscdev.minor=%d\n", DHT_miscdev.minor);
		
	return ret;
}

/*******************************
 *  request and init gpios
 *******************************/

 /*Aquí reservamos el pin del gpio , asociamos y mapeamos la interrupción 
   Para nuestro ejercicio usaremos el pin 18 */
 
static int GPIO_config(void) {
    int res=0;
    
    if ((res=gpio_request(DHT_GPIO, DHT_GPIO_DESC))) {
        printk(KERN_ERR "GPIO request faiure: %s\n", DHT_GPIO_DESC);
        return res;
    }
    if ( (irq_DHT = gpio_to_irq(DHT_GPIO)) < 0 ) {
        printk(KERN_ERR "GPIO to IRQ mapping faiure %s\n", DHT_GPIO_DESC);
        
        return irq_DHT;
    }
    
    printk(KERN_NOTICE "  Mapped int %d for DHT11 in gpio %d\n", irq_DHT, DHT_GPIO);
    
    
    if ((res=request_irq(irq_DHT,
                    (irq_handler_t ) irq_handler_DHT,
                    IRQF_TRIGGER_FALLING,
                    DHT_GPIO_DESC,
                    DHT_GPIO_DEVICE_DESC))) {
        printk(KERN_ERR "Irq Request failure\n");
        return res;
    }
    
	return res;
}

/****************************************************************************/
/* Interruptions                                            				*/
/****************************************************************************/

/*Ésta es la interrupción del DHT11, pasemos a explicarla paso a paso	*/

static irqreturn_t irq_handler_DHT(void){

	dht_buffer[p_buffer++] = ioread32(timer) - cur_timer;	// Vamos guardando en el buffer la diferencia entre el tiempo actual y el último valor del timer
															// Estos valores serán los tiempos que luego transformaremos a decimal para obtener el resultado
															
	cur_timer = ioread32(timer);	// Actualizamos el valor de cur_timer;
	
	/* Una vez que hemos hecho la lectura completa desabilitamos la interrupción
		y damos el paso al procesamiento de datos recibidos */
	if(p_buffer==42) { 
		disable_irq_nosync(irq_DHT);
		up(&sem_wait_processing);
	}	
	
	return IRQ_HANDLED;
}

/****************************************************************************/
/* Module init / cleanup block.                                             */
/****************************************************************************/

/*Función de limpieza donde liberamos los recursos	*/

static void r_cleanup(void) {	
    printk(KERN_NOTICE "%s module cleaning up...\n", KBUILD_MODNAME);
    
    if (DHT_miscdev.this_device) {
		printk("DHT device desregistration...\n");	// Desactivamos los servicios 
		misc_deregister(&DHT_miscdev);
	}
	
    if(irq_DHT) free_irq(irq_DHT, DHT_GPIO_DEVICE_DESC);		// Desmapeamos la interrupción
	gpio_free(DHT_GPIO);										// Liberamos el gpio
		
	/* Liberamos los buffers */
	if(dht_buffer) vfree(dht_buffer);		
	if(str_buffer) vfree(str_buffer);
	if(bits) vfree(bits);
	
	iounmap(timer);		// Desmapeamos el timer 
	
    printk(KERN_NOTICE "Done. Bye from %s module\n", KBUILD_MODNAME);
	
    return;
}

/*En la función init reservamos los recursos e inicializamos las variables	*/

static int r_init(void) {
	
	int res=0;
	
    printk(KERN_NOTICE "Hello, loading %s module!\n", KBUILD_MODNAME);
    printk(KERN_NOTICE "%s - devices config...\n", KBUILD_MODNAME);
    
	/*Creamos los buffers y reservamos su espacio en memoria, cada uno con su tamaño	*/
    dht_buffer = (unsigned*) vmalloc(BUFFER_SIZE*sizeof(u32));
    str_buffer = (char *) vmalloc(BUFFER_SIZE*3*sizeof(char));
    bits = (char *) vmalloc(BUFFER_SIZE*sizeof(char));
	
    if((res = device_config())) {
		r_cleanup();			// Si en la creación del device hay algún fallo se llamará a la función cleanup y se acabará el programa
		return res;
	}
	
    printk(KERN_NOTICE "%s - GPIO config...\n", KBUILD_MODNAME);
    
    if((res = GPIO_config())) {
		r_cleanup();		// Si al pedir y reservar los gpios hay algún problema se llamará a la función cleanup y se acabará el programa
		return res;
	}
	
	disable_irq(irq_DHT);		// La interrupción empieza desabilitada 
	
	timer = ioremap(PI2_TIMER_PAGE_BASE+ TIMER_OFFSET, SZ_4K);		// Mapeamos el timer con su dirección, offset y tamaño
	
	/* bus a VCC por defecto */
	gpio_direction_output(DHT_GPIO, 1);

	/*Al ser éste el comienzo del programa hay que inicializar todas las variables 	*/
	cur_timer = 0;
	p_buffer = 0;
	working = 0;
	humedad_e = 0;
	temperatura_e = 0;
	humedad_d = 0;
	temperatura_d = 0;
	checksum = 0;

	down(&sem_wait_processing);		// Bajamos el semáforo para que pueda entrar un proceso y empezar el programa

    return res;
}

module_init(r_init);
module_exit(r_cleanup);

/****************************************************************************/
/* Module licensing/description block.                                      */
/****************************************************************************/
MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
